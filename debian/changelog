terminus (2.4.2.1-1) unstable; urgency=medium

  * Update debian/watch uscan support due to gitlab churn
  * New upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Mon, 26 Aug 2024 20:55:58 +0100

terminus (2.3.1-1) unstable; urgency=medium

  * New upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 28 Apr 2023 21:43:28 +0100

terminus (2.3.0-1) unstable; urgency=medium

  * New upstream version
  * Refresh man page
  * Update build dependencies per lintian
  * Bump copyright to 2023 per lintian

 -- Barak A. Pearlmutter <bap@debian.org>  Sun, 16 Apr 2023 10:26:07 +0100

terminus (2.2.1-2) unstable; urgency=medium

  * Refresh man page

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 09 Mar 2023 21:39:50 +0000

terminus (2.2.1-1) unstable; urgency=medium

  * New upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 09 Mar 2023 21:20:38 +0000

terminus (2.1.0-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.2, no changes needed.
  * Use dh_installalternatives

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 01 Feb 2023 16:34:54 -0000

terminus (2.0.1-1) unstable; urgency=medium

  * New upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 14 Dec 2022 16:48:25 +0000

terminus (2.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on valac.
  [ Barak A. Pearlmutter ]
  * New upstream version
  * Rebuild man page with help2man

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 06 Dec 2022 13:59:19 +0000

terminus (1.20.0-1) unstable; urgency=medium

  * New upstream version
  * Update man page
  * Update debian/copyright debian/* year

 -- Barak A. Pearlmutter <bap@debian.org>  Mon, 28 Nov 2022 11:45:41 +0000

terminus (1.19.1-1) unstable; urgency=medium

  * New upstream version
  * Remove upstreamed patch

 -- Barak A. Pearlmutter <bap@debian.org>  Mon, 24 Oct 2022 13:16:25 +0100

terminus (1.19.0-1) unstable; urgency=medium

  * New upstream version
  * Include man page in provision of alternative x-terminal-emulator
  * Refresh man page

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 29 Sep 2022 09:53:00 +0100

terminus (1.17.0-2) unstable; urgency=medium

  * Gnome43 patch, thanks to Geremy Bicha

 -- Barak A. Pearlmutter <bap@debian.org>  Sun, 28 Aug 2022 17:39:07 +0100

terminus (1.17.0-1) unstable; urgency=medium

  * New upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 07 Jun 2022 08:30:16 +0100

terminus (1.16.0-1) unstable; urgency=medium

  * New upstream version
  * Remove obsolete patches

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 18 May 2022 20:18:26 +0100

terminus (1.15.1-3) unstable; urgency=medium

  * Upstream patch to set proper $TERM (closes: #1010418)
  * Upstream patch to use correct $SHELL (closes: #955633)

 -- Barak A. Pearlmutter <bap@debian.org>  Mon, 09 May 2022 17:45:11 +0100

terminus (1.15.1-2) unstable; urgency=medium

  * Patch to indent terminus --help text options
  * Use above to regenerate terminus(1)

 -- Barak A. Pearlmutter <bap@debian.org>  Sat, 30 Apr 2022 10:14:55 +0100

terminus (1.15.1-1) unstable; urgency=medium

  * New upstream release

 -- Barak A. Pearlmutter <bap@debian.org>  Sat, 30 Apr 2022 09:38:48 +0100

terminus (1.14.1-1) unstable; urgency=medium

  * New upstream release
  * Remove upstreamed Gnome42 patch

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 01 Apr 2022 11:52:20 +0100

terminus (1.14.0-3) unstable; urgency=medium

  * Gnome42 patch, thanks to Jeremy Bicha

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 25 Mar 2022 20:12:14 +0000

terminus (1.14.0-2) unstable; urgency=medium

  * Remove build dependency on libpcre3-dev (closes: #999944)

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 18 Nov 2021 12:22:39 +0000

terminus (1.14.0-1) unstable; urgency=medium

  * New upstream release
  * Update standards version to 4.6.0, no changes needed.
  * Refresh debian/terminus.1
  * Remove cmake support file from /usr/share/terminus/

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 17 Sep 2021 15:26:41 +0100

terminus (1.13.0-1) unstable; urgency=medium

  * New upstream release

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 03 Dec 2020 10:08:55 +0000

terminus (1.12.0-1) unstable; urgency=medium

  * New upstream release
  * Debhelper 13

 -- Barak A. Pearlmutter <bap@debian.org>  Sat, 23 May 2020 10:52:40 +0100

terminus (1.11.0-1) unstable; urgency=medium

  * New upstream release
  * Refresh terminus(1)

 -- Barak A. Pearlmutter <bap@debian.org>  Mon, 20 Apr 2020 09:14:34 +0100

terminus (1.9.0-2) unstable; urgency=medium

  * Fix help2 issue (don't do it at build time, because chroots/autobuilders.)

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 14 Apr 2020 16:13:41 +0100

terminus (1.9.0-1) unstable; urgency=medium

  * New upstream release
  * Upstream keeps changelog in debian/changelog; move to ChangeLog; keep
    debian/changelog for debian changelog; doing otherwise confuses
    various tools
  * Generate terminus(1) using help2man

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 10 Apr 2020 23:54:48 +0100

terminus (1.8.0-1) unstable; urgency=medium

  * Initial Release (closes: #921601)

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 28 Jan 2020 18:58:40 +0000
